#!/usr/bin/env python3

from contextlib import ExitStack, redirect_stdout
from io import StringIO
from sys import argv
from pytest import main
from types import ModuleType
from typing import List

import pytest
for key in pytest.__dict__:
	if key not in ("__file__", "__name__", "main"):
		globals()[key] = pytest.__dict__[key]

raises_no_exception = ExitStack

def import_file(path: str, name: str) -> ModuleType:
	"""Import a module from a file, even if the file path has no suffix.
	
	Args:
	    name(str): The name to give to the module.
	    path(str): A path to the file containing the module.
	
	Returns:
	    A module with the name `name` built from the contents of the file at
	    the path `path`.
	"""
	from importlib.util import spec_from_loader, module_from_spec
	from importlib.machinery import SourceFileLoader
	from sys import modules
	loader = SourceFileLoader(name, path)
	spec = spec_from_loader(name, loader)
	module = module_from_spec(spec)
	spec.loader.exec_module(module)
	modules[name] = module
	return module

def main(pytest_argv: List[str]=argv[1:]) -> None:
	for index, argument in enumerate(pytest_argv):
		if argument in ("-h", "--help"):
			buffer = StringIO()
			with redirect_stdout(buffer):
				pytest.main(["--help"])
			for line in buffer.getvalue().split("\n"):
				if line.startswith("  --tb=style "):
					print(f"""{line}
  --auto                equivalent to `--tb=auto`.
  --long                equivalent to `--tb=long`.
  --short               equivalent to `--tb=short`.
  --line                equivalent to `--tb=line`.
  --native              equivalent to `--tb=native`.
  --no                  equivalent to `--tb=no`.""")
				else:
					print(f"{line}")
			return 0
		elif argument == "--auto": pytest_argv[index] = "--tb=auto"
		elif argument == "--line": pytest_argv[index] = "--tb=line"
		elif argument == "--long": pytest_argv[index] = "--tb=long"
		elif argument == "--native": pytest_argv[index] = "--tb=native"
		elif argument == "--no": pytest_argv[index] = "--tb=no"
		elif argument == "--short": pytest_argv[index] = "--tb=short"
	pytest.main(pytest_argv)


if __name__ == "__main__":
	main()
