from test import import_file, mark, raises, raises_no_exception
check_help = import_file("check_help", "check_help")

from check_help import Parameters, CLIParameters
from docopt import DocoptExit


class Test_Parameters:

	valid_paths = [
		[],
		["wilma"],
		["wilma", "betty"],
	]

	invalid_paths = [
		"wilma",
		0,
		[1,2],
		{"fred": "wilma", "barney": "betty"},
	]

	def test_valid_construction(self, capsys):
		with raises_no_exception():
			self = Parameters()
			assert isinstance(self, Parameters)
			assert not isinstance(self, list)
			assert hasattr(self, "paths")
			assert isinstance(self.paths, list)
			assert self.paths == []
			assert not hasattr(self, "nonsuch")
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	def test_invalid_construction(self, capsys):
		with raises(TypeError) as exception:
			self = Parameters("nonesuch")
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	@mark.parametrize(
		("paths"),
		[(paths) for paths in valid_paths],
	)
	def test_setattr_path_with_valid_objects(self, capsys, paths):
		with raises_no_exception():
			self = Parameters()
			self.__setattr__("paths", paths)
			assert hasattr(self, "paths")
			assert isinstance(self.paths, list)
			for path in self.paths:
				assert isinstance(path, str)
			assert self.paths == paths
		assert f"=< {paths} >="
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	@mark.parametrize(
		("paths"),
		[(paths) for paths in invalid_paths],
	)
	def test_setattr_path_with_invalid_objects(self, capsys, paths):
		with raises(AttributeError) as exception:
			self = Parameters()
			self.__setattr__("paths", paths)
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	@mark.parametrize(
		("paths"),
		[(paths) for paths in valid_paths],
	)
	def test_set_path_with_valid_objects(self, capsys, paths):
		with raises_no_exception():
			self = Parameters()
			self.paths = paths
			assert hasattr(self, "paths")
			assert isinstance(self.paths, list)
			for path in self.paths:
				assert isinstance(path, str)
			assert self.paths == paths
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	@mark.parametrize(
		("paths"),
		[(paths) for paths in invalid_paths],
	)
	def test_set_path_with_invalid_objects(self, capsys, paths):
		with raises(AttributeError) as exception:
			self = Parameters()
			self.paths = paths
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	def test_setattr_invalid_attribute(self, capsys):
		with raises(AttributeError) as exception:
			self = Parameters()
			self.__setattr__("nonesuch", object)
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	def test_set_invalid_attribute(self, capsys):
		with raises(AttributeError) as exception:
			self = Parameters()
			self.nonesuch = object
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

class Test_CLIParameters:

	valid_no_option_command_lines = [
		[],
		["wilma"],
		["wilma", "betty"]
	]

	valid_short_help_option_command_lines = [
		["-h"],
		["-h", "-h"],
		["-h", "wilma", "betty"],
		["wilma", "-h", "betty"],
		["wilma", "betty", "-h"],
	]
	valid_short_help_option_command_lines += [
		["-x", "--nonesuch"] + command_line
		for command_line
		in valid_short_help_option_command_lines
	]

	valid_long_help_option_command_lines = [
		[
			"--help" if item == "-h" else item
			for item
			in command_line
		]
		for command_line
		in valid_short_help_option_command_lines
	]

	invalid_short_option_command_lines = [
		["-?"],
		["-?", "wilma", "betty"],
		["wilma", "-?", "betty"],
		["wilma", "betty", "-?"],
	]

	invalid_long_option_command_lines = [
		[
			"--nonesuch" if item == "-?" else item
			for item
			in command_line
		]
		for command_line
		in invalid_short_option_command_lines
	]

	@mark.parametrize(
		("command_line"),
		[(command_line) for command_line in valid_no_option_command_lines],
	)
	def test_valid_no_option_command_lines(self, capsys, command_line):
		with raises_no_exception() as exception:
			self = CLIParameters(
				argv=command_line,
			)
			assert self.paths == command_line
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	@mark.parametrize(
		("command_line"),
		[(command_line) for command_line in valid_short_help_option_command_lines],
	)
	def test_short_help_option_command_lines(self, capsys, command_line):
		with raises(SystemExit) as exception:
			self = CLIParameters(
				argv=command_line,
			)
			assert exception.code is None
		stdout, stderr = capsys.readouterr()
		assert stdout == check_help.__doc__
		assert stderr == ""

	@mark.parametrize(
		("command_line"),
		[(command_line) for command_line in valid_long_help_option_command_lines],
	)
	def test_long_help_option_command_lines(self, capsys, command_line):
		with raises(SystemExit) as exception:
			self = CLIParameters(
				argv=command_line,
			)
			assert exception.code is None
		stdout, stderr = capsys.readouterr()
		assert stdout == check_help.__doc__
		assert stderr == ""

	@mark.parametrize(
		("command_line"),
		[(command_line) for command_line in invalid_short_option_command_lines],
	)
	def test_invalid_short_option_command_lines(self, capsys, command_line):
		with raises(DocoptExit) as exception:
			self = CLIParameters(
				argv=command_line,
			)
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""

	@mark.parametrize(
		("command_line"),
		[(command_line) for command_line in invalid_long_option_command_lines],
	)
	def test_invalid_long_option_command_lines(self, capsys, command_line):
		with raises(DocoptExit) as exception:
			self = CLIParameters(
				argv=command_line,
			)
		stdout, stderr = capsys.readouterr()
		assert stdout == ""
		assert stderr == ""
